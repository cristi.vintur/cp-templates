#include <bits/stdc++.h>
using namespace std;
using uint = unsigned int;
using ll = long long;
using ld = long double;
using ull = unsigned long long;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
#define dbg(x) cerr<<#x": "<<(x)<<endl
#define dbg_p(x) cerr<<#x": "<<(x).first<<' '<<(x).second<<endl
#define dbg_v(x, n) {cerr<<#x"[]: ";for(long long _=0;_<n;++_)cerr<<(x)[_]<<' ';cerr<<endl;}
#define all(v) v.begin(), v.end()
#define fi first
#define se second

template<typename T1, typename T2>
ostream& operator <<(ostream &out, const pair<T1, T2> &item) {
	out << '(' << item.first << ", " << item.second << ')';
	return out;
}

template <typename T>
ostream& operator <<(ostream &out, const vector<T>& v) {
	for(const auto &item : v) out << item << ' ';
	return out;
}

const int N = 200010;
const int V = 1000010;

struct PersistentST {
	ll s;
	PersistentST *l, *r;

	PersistentST(ll s = 0) : s(s) { l = r = nullptr; }
	PersistentST(PersistentST *pst) { s = pst->s; l = pst->l; r = pst->r; }

	ll gets(PersistentST *node) { return node ? node->s : 0; }

	PersistentST* update(int l, int r, int p, ll val) {
		if(l == r) return new PersistentST(this->s + val);

		int mid = (l + r) / 2;
		PersistentST *res = new PersistentST(this);
		if(p <= mid) {
			if(!res->l) res->l = new PersistentST();
			res->l = res->l->update(l, mid, p, val);
		} else {
			if(!res->r) res->r = new PersistentST();
			res->r = res->r->update(mid + 1, r, p, val);
		}

		res->s = gets(res->l) + gets(res->r);

		return res;
	}

	ll query(int l, int r, int a, int b) {
		if(a <= l && r <= b) return this->s;

		int mid = (l + r) / 2;
		ll vl = (this->l && a <= mid ? this->l->query(l, mid, a, b) : 0);
		ll vr = (this->r && mid + 1 <= b ? this->r->query(mid + 1, r, a, b) : 0);

		return vl + vr;
	}

	void clear() {
		if(this->l) this->l->clear();
		if(this->r) this->r->clear();
		delete this;
	}
};

int v[N], nxtLs[N], nxtGr[N];
vector<pii> points[V];
PersistentST *rev[V];

void clearAll() {
	for(int i = 0; i < V; ++i) points[i].clear();
	for(int i = 0; i < V; ++i) rev[i]->clear();
}

void addPoint(int p, pll val) {
	points[p].push_back(val);
}

void preprocess(int n) {
	stack<int> stk;

	// creating nextLess and nextGreater arrays
	for(int i = n - 1; i >= 0; --i) {
		while(!stk.empty() && v[stk.top()] >= v[i]) stk.pop();
		nxtLs[i] = (stk.empty() ? n : stk.top());
		stk.push(i);
	}

	while(!stk.empty()) stk.pop();
	for(int i = n - 1; i >= 0; --i) {
		while(!stk.empty() && v[stk.top()] <= v[i]) stk.pop();
		nxtGr[i] = (stk.empty() ? n : stk.top());
		stk.push(i);
	}

	// creating all distinct segments: (mn, mx) - minimum and maximum on intervals
	for(int i = 0; i < n; ++i) {
		int mn = v[i], mx = v[i], mni = i, mxi = i;
		for(int j = i; j < n; ) {
			int p = min(nxtLs[mni], nxtGr[mxi]);
			// new point (mn, mx) covering [j, p) => size p - j
			addPoint(mn, { mx, p - j });
			j = p;
			if(mni < n && v[p] < mn) mn = v[mni = p];
			if(mxi < n && v[p] > mx) mx = v[mxi = p];
		}
	}

	rev[0] = new PersistentST();
	for(int i = 1; i < V; ++i) {
		rev[i] = rev[i - 1];

		for(auto [x, y] : points[i])
			rev[i] = rev[i]->update(0, V - 1, x, y);
	}
}

ll query(int mn, int mx) {
	return rev[mx]->query(0, V - 1, mn, mx) - rev[mn - 1]->query(0, V - 1, mn, mx);
}

int main()
{
	ios_base::sync_with_stdio(false);
	
	int t, n, k, l, h;

	for(cin >> t; t; --t) {
		cin >> n >> k;
		for(int i = 0; i < n; ++i) cin >> v[i];

		preprocess(n);
		
		while(k--) {
			cin >> l >> h;
			cout << query(l, h) << '\n';
		}

		clearAll();
	}

	return 0;
}
