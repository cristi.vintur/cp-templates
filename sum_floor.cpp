// returns [p/q] + [2p/q] + ... + [np/q]
ll solve(ll p, ll q, ll n) {
	ll g, res, z;
	
	g = gcd(p, q);
	p /= g;
	q /= g;
	
	for(res = 0, z = 1; q && n; swap(p, q), z = -z) {
		ll t = p / q;
		res += z * t * n * (n + 1) / 2;
		p %= q;
		
		t = n / q;
		res += z * p * t * (n + 1) - z * t * (p * q * t + p + q - 1) / 2;
		n %= q;
		
		t = (n * p) / q;
		res += z * t * n;
		n = t;
	}
	
	return res;
}

