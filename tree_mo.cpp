#include <bits/stdc++.h>
using namespace std;
using uint = unsigned int;
using ll = long long;
using ld = long double;
using ull = unsigned long long;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
#define dbg(...) cerr << #__VA_ARGS__ << " ->", debug_out(__VA_ARGS__)
#define dbg_p(x) cerr<<#x": "<<(x).first<<' '<<(x).second<<endl
#define dbg_v(x, n) {cerr<<#x"[]: ";for(long long _=0;_<n;++_)cerr<<(x)[_]<<' ';cerr<<endl;}
#define all(v) v.begin(), v.end()
#define fi first
#define se second
void debug_out() { cerr << endl; }
template <typename Head, typename... Tail> void debug_out(Head H, Tail... T) { cerr << " " << H; debug_out(T...);}

template<typename T1, typename T2>
ostream& operator <<(ostream &out, const pair<T1, T2> &item) {
	out << '(' << item.first << ", " << item.second << ')';
	return out;
}

template <typename T>
ostream& operator <<(ostream &out, const vector<T>& v) {
	for(const auto &item : v) out << item << ' ';
	return out;
}

const int LOGN = 17;
const int B = 350;
const int N = 200010;
const int Q = 100010;
const int V = 1000010;
const int MOD = 1000000007;

int bl[N];

struct Query {
	int l, r, lca, id;
	bool operator <(const Query &o) const {
		return (bl[l] != bl[o.l] ? bl[l] < bl[o.l] : ((r < o.r) ^ (bl[l] & 1)));
	}
};

int t, curr;
int alpha[V], tin[N], tout[N], node[N], occ[N], h[N];
int ans[Q], preInv[20 * N];
int fth[LOGN][N];
vector<int> adj[N];
vector<pii> divs[N];

int pw(int base, int exp, int mod) {
	int res;
	for(res = 1; exp; exp >>= 1) {
		if(exp & 1) res = (1LL * res * base) % mod;
		base = (1LL * base * base) % mod;
	}
	
	return res;
}

int modInv(int base, int mod) {
	return pw(base, mod - 2, mod);
}

void process(int i, int x) {
	divs[i].clear();
	for(int d = 2; d * d <= x; ++d)
		if(x % d == 0) {
			int e = 0;
			while(x % d == 0) x /= d, ++e;
			divs[i].push_back({d, e});
		}
	if(x > 1) divs[i].push_back({x, 1});
}

void dfs(int v, int p = -1) {
	tin[v] = ++t;
	node[t] = v;

	for(auto u : adj[v]) {
		if(u == p) continue;
		fth[0][u] = v;
		h[u] = 1 + h[v];
		dfs(u, v);
	}

	tout[v] = ++t;
	node[t] = v;
}

int getLCA(int v, int u) {
	if(h[v] > h[u]) swap(v, u);
	for(int i = LOGN - 1; i >= 0; --i)
		if(h[fth[i][u]] >= h[v]) u = fth[i][u];
	if(v == u) return v;

	for(int i = LOGN - 1; i >= 0; --i)
		if(fth[i][v] != fth[i][u]) v = fth[i][v], u = fth[i][u];
	return fth[0][v];
}

void precompute(int n) {
	t = 0;
	h[1] = 1;
	dfs(1);

	for(int i = 1; i < LOGN; ++i)
		for(int j = 1; j <= n; ++j)
			fth[i][j] = fth[i - 1][fth[i - 1][j]];
}

void addNode(int v, int mul) {
	//dbg(v, mul);
	for(const auto &item : divs[v]) {
		//dbg(item);
		int p = item.fi;
		int e = item.se;
		curr = (1LL * curr * preInv[1 + alpha[p]]) % MOD;
		alpha[p] += e * mul;
		curr = (1LL * curr * (1 + alpha[p])) % MOD;
	}
}

void add(int v) {
	++occ[v];
	if(occ[v] == 1) addNode(v, 1);
	else addNode(v, -1);
}

void erase(int v) {
	--occ[v];
	if(occ[v] == 0) addNode(v, -1);
	else addNode(v, 1);
}

int main()
{
	ios_base::sync_with_stdio(false);
	
	int t, n, a, b, x, q;

	for(int i = 0; i < N; ++i) bl[i] = i / B;
	for(int i = 1; i < 20 * N; ++i) preInv[i] = modInv(i, MOD);

	for(cin >> t; t; --t) {
		cin >> n;
		for(int i = 1; i < n; ++i) {
			cin >> a >> b;
			adj[a].push_back(b);
			adj[b].push_back(a);
		}

		for(int i = 1; i <= n; ++i) {
			cin >> x;
			process(i, x);
		}

		precompute(n);
		memset(alpha, 0, sizeof alpha);
		memset(occ, 0, sizeof occ);

		cin >> q;
		vector<Query> queries;
		for(int i = 0; i < q; ++i) {
			cin >> a >> b;
			if(tin[a] > tin[b]) swap(a, b);

			if(tout[b] <= tout[a]) { // lca(a, b) == a
				queries.push_back({tin[a], tin[b], 0, i});
			} else {
				queries.push_back({tout[a], tin[b], getLCA(a, b), i});
			}
		}

		sort(all(queries));

		curr = 1;

		int l = 1, r = 0;
		for(const auto &item : queries) {
			int ql = item.l;
			int qr = item.r;
			int lca = item.lca;
			int id = item.id;

			//dbg(ql, qr);
			//dbg(id, ql, qr);
			while(r < qr) add(node[++r]);
			while(l > ql) add(node[--l]);
			while(r > qr) erase(node[r--]);
			while(l < ql) erase(node[l++]);

			if(lca) {
				addNode(lca, 1);
				ans[id] = curr;
				addNode(lca, -1);
			} else {
				ans[id] = curr;
			}
		}

		for(int i = 0; i < q; ++i) cout << ans[i] << '\n';

		for(int i = 1; i <= n; ++i) adj[i].clear();
	}

	return 0;
}

