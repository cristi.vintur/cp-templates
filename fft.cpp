struct ftype {
	double a, b;
	
	ftype(double a = 0, double b = 0) : a(a), b(b) {}
	ftype conj() { return ftype(a, -b); }
	friend ftype operator +(const ftype &x, const ftype &y) { return ftype(x.a + y.a, x.b + y.b); }
	friend ftype operator -(const ftype &x, const ftype &y) { return ftype(x.a - y.a, x.b - y.b); }
	friend ftype operator *(const ftype &x, const ftype &y) { return ftype(x.a * y.a - x.b * y.b, x.a * y.b + x.b * y.a); }
	friend ftype operator /(const ftype &x, int y) { return ftype(x.a / y, x.b / y); }
};

const double PI = acos(-1);

ftype polar(double ang) { return ftype(cos(ang), sin(ang)); }

int rv(int x, int sz) {
	int ans = 0;
	for(int i = 0; i < sz; ++i)
		if(x & (1 << i)) ans |= (1 << (sz - 1 - i));

	return ans;
}

vector<ftype> fft(vector<ftype> p, bool rev = false) {
	int i, sz, n = p.size();
	for(sz = 0; (1 << sz) < n; ++sz);

	for(int i = 0; i < n; ++i)
		if(i < rv(i, sz)) swap(p[i], p[rv(i, sz)]);

	for(int len = 2; len <= n; len <<= 1) {
		ftype wlen = polar((rev ? -1 : 1) * 2 * PI / len);

		for(int i = 0; i < n; i += len) {
			ftype w = 1;
			for(int j = 0; j < len / 2; ++j) {
				ftype u = p[i + j] + w * p[i + j + len / 2];
				ftype v = p[i + j] - w * p[i + j + len / 2];
				p[i + j] = u;
				p[i + j + len / 2] = v;
				w = w * wlen;
			}
		}
	}

	if(rev) {
		for(auto &val : p) val.a /= p.size(), val.b /= p.size();
	}

	return p;
}

vector<int> multiply(const vector<int> &a, const vector<int> &b) {
	int sz = 2 * max(a.size(), b.size());
	while(__builtin_popcount(sz) != 1) ++sz;

	vector<ftype> na, nc;

	na.resize(sz);
	for(int i = 0; i < a.size(); ++i) na[i].a = a[i];
	for(int i = 0; i < b.size(); ++i) na[i].b = b[i];

	auto r = fft(na);

	for(int i = 0; i < r.size(); ++i) {
		ftype x = r[i];
		ftype y = r[i == 0 ? i : r.size() - i].conj();
		ftype ai = (x + y) / 2;
		ftype bi = (x - y) / 2 * ftype(0, -1);
		nc.push_back(ai * bi);
	}

	auto vc = fft(nc, true);

	vector<int> c;
	for(auto val : vc) c.push_back(round(val.a));

	return c;
}
