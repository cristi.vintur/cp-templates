#include <bits/stdc++.h>
using namespace std;
using uint = unsigned int;
using ll = long long;
using ld = long double;
using ull = unsigned long long;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
#define dbg(...) cerr << #__VA_ARGS__ << " ->", debug_out(__VA_ARGS__)
#define dbg_p(x) cerr<<#x": "<<(x).first<<' '<<(x).second<<endl
#define dbg_v(x, n) {cerr<<#x"[]: ";for(long long _=0;_<n;++_)cerr<<(x)[_]<<' ';cerr<<endl;}
#define all(v) v.begin(), v.end()
#define fi first
#define se second
void debug_out() { cerr << endl; }
template <typename Head, typename... Tail> void debug_out(Head H, Tail... T) { cerr << " " << H; debug_out(T...);}

template<typename T1, typename T2>
ostream& operator <<(ostream &out, const pair<T1, T2> &item) {
	out << '(' << item.first << ", " << item.second << ')';
	return out;
}

template <typename T>
ostream& operator <<(ostream &out, const vector<T>& v) {
	for(const auto &item : v) out << item << ' ';
	return out;
}

const int A = 26;
const int B = 137;
const int N = 400010;
const int L = 800010;

struct PalTree {
	int last;
	string s;
	vector<int> len, suff;
	vector<vector<int>> nxt;

	PalTree() {
		last = 0;

		len.push_back(-1);
		suff.push_back(0);

		len.push_back(0);
		suff.push_back(0);
		nxt.assign(2, vector<int>(A, -1));
	}

	void addLetter(char c) {
		s += c;

		int id = last;
		while(len[id] == s.length() - 1 || s[s.length() - 2 - len[id]] != c) id = suff[id];
		if(nxt[id][c - 'a'] >= 0) return void(last = nxt[id][c - 'a']);

		nxt[id][c - 'a'] = len.size();
		last = len.size();
		len.push_back(len[id] + 2);
		nxt.push_back(vector<int>(A, -1));
		if(len.back() == 1) return void(suff.push_back(1));
		
		for(id = suff[id]; len[id] == s.length() - 1 || s[s.length() - 2 - len[id]] != c; id = suff[id]);
		suff.push_back(nxt[id][c - 'a']);
	}
};

ull pw[L], equivClass[N];
PalTree pal[N];
vector<ull> h[N];
set<ull> hashes[N];
map<ull, int> sz;

ull getHash(const vector<ull> &h, int l, int r) {
	return h[r + 1] - h[l] * pw[r - l + 1];
}

void add(int i, char c) {
	pal[i].addLetter(c);
	h[i].push_back(h[i].back() * B + c);

	int len = pal[i].len[pal[i].last];
	ull hh = getHash(h[i], pal[i].s.size() - len, pal[i].s.size() - 1);
	if(!hashes[i].count(hh)) {
		equivClass[i] += hh * hh;
		hashes[i].insert(hh);
	}
}

int main()
{
	ios_base::sync_with_stdio(false);

	int m, type, i;
	char c;

	pw[0] = 1;
	for(int i = 1; i < L; ++i) pw[i] = pw[i - 1] * B;
	for(int i = 0; i < N; ++i) h[i].push_back(0);

	int nr = 0;
	for(scanf("%d", &m); m; --m) {
		scanf("%d %d", &type, &i);
		--i;

		if(type == 1) {
			scanf(" %c", &c);

			if(i < nr) --sz[equivClass[i]];
			add(i, c);
			++sz[equivClass[i]];

			nr = max(nr, i + 1);
		} else {
			cout << sz[equivClass[i]] << '\n';
		}
	}

	return 0;
}

