struct Sat {
	int n;
	vector<bool> vis;
	vector<int> ord, comp, val;
	vector<vector<int>> adj, adjt, nodes;

	Sat(int n) : n(n), adj(2 * n), adjt(2 * n), nodes(2 * n) {}

	void addEdge(int x, int y) {
		x = (x > 0 ? 2 * x - 1 : -2 * x - 2);
		y = (y > 0 ? 2 * y - 1 : -2 * y - 2);
		adj[x].push_back(y);
		adjt[y].push_back(x);
	}

	void addClause(int x, int y) {
		addEdge(-x, y);
		addEdge(-y, x);
	}

	void dfs(int v) {
		vis[v] = true;
		for(auto u : adj[v]) if(!vis[u]) dfs(u);
		ord.push_back(v);
	}

	void dfst(int v, int nr) {
		vis[v] = false;
		comp[v] = nr;
		nodes[nr].push_back(v);
		for(auto u : adjt[v]) if(vis[u]) dfst(u, nr);
	}

	bool solve() {
		int nr = 0;
		vis.assign(2 * n, false);
		comp.assign(2 * n, -1);
		for(int i = 0; i < 2 * n; ++i) if(!vis[i]) dfs(i);
		for(int i = 2 * n - 1; i >= 0; --i) if(vis[ord[i]]) dfst(ord[i], nr++);
		for(int i = 0; i < 2 * n; ++i) if(comp[i] == comp[i ^ 1]) return false;

		vector<int> in(nr, 0);
		for(int i = 0; i < 2 * n; ++i) for(auto u : adj[i]) if(comp[i] != comp[u]) ++in[comp[u]];
		
		val.assign(2 * n, -1);
		
		queue<int> q;
		for(int i = 0; i < nr; ++i) if(!in[i]) q.push(i);
		for(; !q.empty(); q.pop()) {
			for(auto v : nodes[q.front()]) {
				if(val[v] == -1) val[v] = 0, val[v ^ 1] = 1;			
				for(auto u : adj[v]) if(comp[v] != comp[u] && (--in[comp[u]]) == 0) q.push(comp[u]);
			}
		}

		return true;
	}

	bool get(int i) {
		return val[2 * i - 1];
	}
};

