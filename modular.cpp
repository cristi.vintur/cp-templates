template<int MOD = 1000000007>
struct ModInt {
	int x;
	
	ModInt(int x = 0) : x(x) {}
	
	ModInt& operator +=(const ModInt &a) { if((x += a.x) >= MOD) x -= MOD; return *this; }
	ModInt& operator -=(const ModInt &a) { if((x -= a.x) < 0) x += MOD; return *this; }
	ModInt& operator *=(const ModInt &a) { x = (1LL * x * a.x) % MOD; return *this; }
	ModInt& operator /=(const ModInt &a) { *this = (*this) * getPw(a, MOD - 2); return *this; }
	
	
	friend ModInt operator +(const ModInt &a, const ModInt &b) { return ModInt(a.x + b.x >= MOD ? a.x + b.x - MOD : a.x + b.x); }
	friend ModInt operator -(const ModInt &a, const ModInt &b) { return ModInt(a.x < b.x ? a.x - b.x + MOD : a.x - b.x); }
	friend ModInt operator *(const ModInt &a, const ModInt &b) { return ModInt((1LL * a.x * b.x) % MOD); }
	friend ModInt operator /(const ModInt &a, const ModInt &b) { return a * getPw(b, MOD - 2); }
	
	friend ModInt operator -(const ModInt &a) { return ModInt(a.x == 0 ? 0 : MOD - a.x); }
	
	friend ModInt getPw(ModInt a, int b) {
		ModInt ans = 1;
		for(; b; b /= 2) {
			if(b & 1) ans *= a;
			a *= a;
		}
		return ans;
	}
	
	bool operator <(const ModInt &other) const { return x < other.x; }
	bool operator ==(const ModInt &other) const { return x == other.x; }
	
	friend ostream& operator <<(ostream &out, const ModInt &a) { return out << a.x; }
	
	operator int() { return x; }
};

