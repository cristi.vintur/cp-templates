#include <bits/stdc++.h>
using namespace std;
using uint = unsigned int;
using ll = long long;
using ull = unsigned long long;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
#define dbg(x) cerr<<#x": "<<(x)<<endl
#define dbg_p(x) cerr<<#x": "<<(x).first<<' '<<(x).second<<endl
#define dbg_v(x, n) {cerr<<#x"[]: ";for(long long _=0;_<n;++_)cerr<<(x)[_]<<' ';cerr<<endl;}
#define all(v) v.begin(), v.end()
#define fi first
#define se second

template<typename T1, typename T2>
ostream& operator <<(ostream &out, const pair<T1, T2> &item) {
	out << '(' << item.first << ", " << item.second << ')';
	return out;
}

template <typename T>
ostream& operator <<(ostream &out, const vector<T>& v) {
	for(const auto &item : v) out << item << ' ';
	return out;
}

const int N = 2000010;
const int A = 26;

int k, last;
int len[N], link[N];
int go[N][A];

void addLetter(int ch) {
	int p = last;
	last = k++;
	len[last] = len[p] + 1;

	while(!go[p][ch]) go[p][ch] = last, p = link[p];
	if(go[p][ch] == last) return void(link[last] = 0);

	int q = go[p][ch];
	if(len[q] == len[p] + 1) return void(link[last] = q);

	int cl = k++;
	memcpy(go[cl], go[q], sizeof go[q]);
	link[cl] = link[q];
	len[cl] = len[p] + 1;
	link[last] = link[q] = cl;

	while(go[p][ch] == q) go[p][ch] = cl, p = link[p];
}

int main()
{
	ios_base::sync_with_stdio(false);
	
	string s, t;

	k = 1;

	cin >> s >> t;
	for(auto ch : t) addLetter(ch - 'a');

	/*for(int i = 0; i < k; ++i) {
		cout << i << ' ' << link[i] << ' ' << len[i] << '\n';
		for(int j = 0; j < A; ++j) if(go[i][j]) cout << char('a' + j) << ' ' << go[i][j] << '\n';
		cout << '\n';
	}*/

	int node = 0, n = s.length(), ans = -n, ansi = -1, ansj = -1;
	for(int i = 0, j = 0; i < n; ++i) {
		if(j < i) j = i;
		while(j < n && go[node][s[j] - 'a']) node = go[node][s[j] - 'a'], ++j;
		if(i < j && ans < j - i - max(i, n - j)) ans = j - i - max(i, n - j), ansi = i, ansj = j - 1;

		//cerr << i << ' ' << j << '\n';
		if(j - i == len[link[node]] + 1) node = link[node];
	}

	cout << ansi << ' ' << ansj << '\n';

	return 0;
}
