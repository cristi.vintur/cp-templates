int pw(int base, int exp, int mod) {
	int res;
	for(res = 1; exp; exp >>= 1) {
		if(exp & 1) res = (1LL * res * base) % mod;
		base = (1LL * base * base) % mod;
	}
	
	return res;
}

int modInv(int base, int mod) {
	return pw(base, mod - 2, mod);
}

